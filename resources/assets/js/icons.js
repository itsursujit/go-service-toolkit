import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUsers,
  faPencilAlt,
  faTrashAlt,
  faPlus,
  faUserCog,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faUsers,
  faPencilAlt,
  faTrashAlt,
  faPlus,
  faUserCog
)