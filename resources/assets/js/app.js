import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import { VuejsDatatableFactory } from 'vuejs-datatable';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueSwal from 'vue-swal'

import store from './store'
import routes from './routes'
import "./icons"

Vue.component('FontAwesome', FontAwesomeIcon)
Vue.config.productionTip = false

Vue.use(Router)
Vue.use(VueSwal)

VuejsDatatableFactory.useDefaultType(false)
  .registerTableType('datatable', tableType => tableType
    .mergeSettings({
      table: { class: 'table-fixed w-full' },
      pager: {
        classes: {
          pager: 'mt-2 flex',
          li: 'mx-1 px-3 py-2 text-white bg-gray-500 hover:bg-gray-700 rounded',
        }
      }
    }));
Vue.use(VuejsDatatableFactory);

const router = new Router({
  mode: 'hash',
  routes
})
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

