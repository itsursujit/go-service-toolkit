import Axios from "axios";

export const GetRoleList = () => Axios.get(`/roles`).then(r => r.data)

export const GetSingleRole = (id) => Axios.get(`/roles`).then(r => {
  const { data: d } = r
  const { data: datas } = d
  return new Promise(r => {
    for (const data of datas) {
      if (data.id == id) {
        r(data)
      }
    }
    r(null)
  })
})

export const DeleteRole = (id) => Axios.delete(`/roles`, {
  data: {
    id: parseInt(id)
  }
}).then(r => r.data)

export const UpdateRole = (id, payload) => Axios.put(`/roles`, {
  id: parseInt(id),
  ...payload
}).then(r => r.data)

export const CreateRole = payload => Axios.post(`/roles`, payload).then(r => r.data)