import Axios from "axios";

export const GetUserList = () => Axios.get(`/users`).then(r => r.data)

export const GetSingleUser = (id) => Axios.get(`/users`).then(r => {
  const { data: d } = r
  const { data: datas } = d
  return new Promise(r => {
    for (const data of datas) {
      if (data.id == id) {
        r(data)
      }
    }
    r(null)
  })
})

export const DeleteUser = (id) => Axios.delete(`/users`, {
  data: {
    id: parseInt(id),
  }
}).then(r => r.data)

export const UpdateUser = (id, payload) => Axios.put(`/users`, {
  id: parseInt(id),
  ...payload
}).then(r => r.data)

export const CreateUser = (data) => Axios.post(`/users`, data).then(r => r.data)