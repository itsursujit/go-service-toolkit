import Dashboard from './components/Dashboard'
import DashboardHome from './pages/Home'
import UserPage from './pages/User'
import UserFormPage from './pages/UserForm'
import RolePage from './pages/Role'
import RoleFormPage from './pages/RoleForm'

const routes = [
  { path: '/', redirect: { name: 'DashboardHome' } },
  {
    path: '/dashboard', component: Dashboard, children: [
      { path: '/', redirect: { name: 'DashboardHome' } },
      { path: 'home', name: 'DashboardHome', component: DashboardHome },
      { path: 'user', name: 'UserPage', component: UserPage },
      { path: 'user/edit', redirect: { name: 'UserPage' } },
      { path: 'user/edit/:id', name: 'UserEdit', component: UserFormPage },
      { path: 'user/create', name: 'UserCreate', component: UserFormPage },
      { path: 'role', name: 'RolePage', component: RolePage },
      { path: 'role/edit', redirect: { name: 'RolePage' } },
      { path: 'role/edit/:id', name: 'RoleEdit', component: RoleFormPage },
      { path: 'role/create', name: 'RoleCreate', component: RoleFormPage }

    ]
  }
]

export default routes