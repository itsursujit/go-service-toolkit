FROM golang:1.14.4-alpine3.12 as builder
RUN go version
WORKDIR /go/src/go-service-toolkit
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o go-service-toolkit .

FROM scratch
COPY --from=builder /go/src/go-service-toolkit/go-service-toolkit /app/
COPY --from=builder /go/src/go-service-toolkit/rbac.conf /app/
COPY --from=builder /go/src/go-service-toolkit/.env /app/
COPY --from=builder /go/src/go-service-toolkit/migrations/ /app/migrations/
COPY --from=builder /go/src/go-service-toolkit/resources/ /app/resources/
COPY --from=builder /go/src/go-service-toolkit/static/ /app/static/

WORKDIR /app
CMD ["./go-service-toolkit"]

