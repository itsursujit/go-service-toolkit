package users

import (
	"fmt"
	models "toolkit/app/app/Models"
)

func (svc *Initial) GetUserServices() models.Response {
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	users := []models.GetUserData{}
	sql := `SELECT users.id, users.name, roles.name as role FROM users 
		INNER JOIN user_roles ON users.id = user_roles.user_id 
		INNER JOIN roles ON user_roles.role_id = roles.id`
	rows, err := svc.DB.Raw(sql).Rows()
	defer rows.Close()
	if err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to get data from db: %s", err.Error()))
		return response
	}
	for rows.Next() {
		user := models.GetUserData{}
		if err := rows.Scan(
			&user.ID,
			&user.Name,
			&user.Role,
		); err != nil {
			svc.Obs.Logger.Error(fmt.Sprintf("Failed to scan data from db: %s", err.Error()))
			return response
		}
		users = append(users, user)
	}

	response.Code = "00"
	response.Message = "Succesful"
	response.Data = users
	return response
}
