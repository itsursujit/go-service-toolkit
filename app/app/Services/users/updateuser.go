package users

import (
	"crypto/md5"
	"fmt"
	models "toolkit/app/app/Models"
)

func (svc *Initial) UpdateUserServices(updateUser models.User) models.Response {
	res := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	if updateUser.Password != "" {
		updateUser.Password = fmt.Sprintf("%x", md5.Sum([]byte(updateUser.Password)))
		if err := svc.DB.Exec("update users set name = ?, password = ? where id = ?", updateUser.Name, updateUser.Password, updateUser.ID).Error; err != nil {
			svc.Obs.Logger.Error(fmt.Sprintf("Failed to update data db: %s", err.Error()))
			return res
		}
		if err := svc.DB.Exec("update accounts set name = ? where id = ?", updateUser.Name, updateUser.ID).Error; err != nil {
			svc.Obs.Logger.Error(fmt.Sprintf("Failed to update data db: %s", err.Error()))
			return res
		}
	} else {
		if err := svc.DB.Exec("update users set name = ? where id = ?", updateUser.Name, updateUser.ID).Error; err != nil {
			svc.Obs.Logger.Error(fmt.Sprintf("Failed to update data db: %s", err.Error()))
			return res
		}
		if err := svc.DB.Exec("update accounts set name = ? where id = ?", updateUser.Name, updateUser.ID).Error; err != nil {
			svc.Obs.Logger.Error(fmt.Sprintf("Failed to update data db: %s", err.Error()))
			return res
		}
	}
	res.Code = "00"
	res.Message = "Succesful"
	return res
}
