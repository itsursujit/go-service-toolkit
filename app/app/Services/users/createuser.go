package users

import (
	"crypto/md5"
	"fmt"
	models "toolkit/app/app/Models"
	"toolkit/app/core/cache"
	"toolkit/app/core/observance"

	"github.com/jinzhu/gorm"
)

type Initial struct {
	Obs   *observance.Obs
	DB    *gorm.DB
	Cache cache.Cache
}

func (svc *Initial) CreateUserServices(req models.CreateUser) models.Response {
	res := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	user := models.User{
		Name:     req.Name,
		Password: fmt.Sprintf("%x", md5.Sum([]byte(req.Password))),
	}
	if err := svc.DB.Save(&user).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to save db: %s", err.Error()))
		return res
	}
	account := models.Account{Name: req.Name}
	if err := svc.DB.Save(&account).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to save db: %s", err.Error()))
		return res
	}
	userRole := models.UserRole{
		UserID:    user.ID,
		AccountID: account.ID,
		RoleID:    req.RoleID,
	}
	if err := svc.DB.Save(&userRole).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to save db: %s", err.Error()))
		return res
	}

	res.Code = "00"
	res.Message = "Succesful"
	return res
}
