package users

import (
	"fmt"
	models "toolkit/app/app/Models"
)

func (svc *Initial) DeleteUserServices(deleteUser models.User) models.Response {
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	if err := svc.DB.Exec("delete from users where id = ? ", deleteUser.ID).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to delete data db: %s", err.Error()))
		return response
	}

	if err := svc.DB.Exec("delete from accounts where id = ? ", deleteUser.ID).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to delete data db: %s", err.Error()))
		return response
	}

	if err := svc.DB.Exec("delete from user_roles where user_id = ? ", deleteUser.ID).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to delete data db: %s", err.Error()))
		return response
	}

	response.Code = "00"
	response.Message = "Succesful"
	return response
}
