package roles

import (
	"fmt"
	models "toolkit/app/app/Models"
)

func (svc *Initial) GetRoleServices() models.Response {
	res := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	data := []models.Role{}
	if err := svc.DB.Find(&data).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to save db: %s", err.Error()))
		return res
	}

	res.Code = "00"
	res.Message = "Succesful"
	res.Data = data

	return res
}
