package roles

import (
	"fmt"
	"os"
	models "toolkit/app/app/Models"
	"toolkit/app/core/cache"
	"toolkit/app/core/observance"

	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v2"
	"github.com/jinzhu/gorm"
)

type Initial struct {
	Obs   *observance.Obs
	DB    *gorm.DB
	Cache cache.Cache
}

func (svc *Initial) CreateRoleServices(req models.Role) models.Response {
	res := models.Response{
		Code:    "01",
		Message: "Failed",
	}

	adapter, err := gormadapter.NewAdapter("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/", os.Getenv("DATABASE_USER"), os.Getenv("DATABASE_PASSWORD"), os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_PORT")))
	if err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to get new casbin adapter: %s", err))
		return res
	}

	cas, err := casbin.NewEnforcer("rbac_model.conf", adapter)
	if err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to create new enforce: %s", err))
		return res
	}

	if err := svc.DB.Save(&req).Error; err != nil {
		svc.Obs.Logger.Error(fmt.Sprintf("Failed to save db: %s", err.Error()))
		return res
	}

	go func() {
		for _, val := range req.Permissions {
			cas.AddPolicy(req.Name, val.Path, val.Method)
		}
	}()

	res.Code = "00"
	res.Message = "Succesful"

	return res
}
