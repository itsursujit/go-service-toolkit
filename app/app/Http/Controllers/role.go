package controllers

import (
	"fmt"
	"net/http"

	models "toolkit/app/app/Models"
	"toolkit/app/app/Services/roles"
)

func (c *Initial) CreateRole() {
	c.Obs.Logger.Info("incoming request to to list al; users")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}

	data := models.Role{}
	if err := c.Fiber.BodyParser(&data); err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := roles.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.CreateRoleServices(data)

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) GetRole() {
	c.Obs.Logger.Info("incoming request to to list al; users")

	svc := roles.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response := svc.GetRoleServices()

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) UpdateRole() {
	c.Obs.Logger.Info("incoming request to to list al; users")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}

	data := models.Role{}
	if err := c.Fiber.BodyParser(&data); err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := roles.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.UpdateRoleServices(data)

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) DeleteRole() {
	c.Obs.Logger.Info("incoming request to to list al; users")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}

	data := models.Role{}
	if err := c.Fiber.BodyParser(&data); err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := roles.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.DeleteRoleServices(data)

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}
