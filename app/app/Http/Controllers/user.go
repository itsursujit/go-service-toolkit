package controllers

import (
	"fmt"
	"net/http"
	models "toolkit/app/app/Models"
	services "toolkit/app/app/Services/users"
	"toolkit/app/core/cache"
	"toolkit/app/core/observance"

	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

type Initial struct {
	Obs   *observance.Obs
	Fiber *fiber.Ctx
	DB    *gorm.DB
	Cache cache.Cache
}

func (c *Initial) CreateUser() {
	c.Obs.Logger.Info("incoming request to create new user")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	newUser := models.CreateUser{}
	err := c.Fiber.BodyParser(&newUser)
	if err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := services.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.CreateUserServices(newUser)

	// Nonsense cache usage example
	c.Cache.SetJSON("latestNewUser", newUser, 0)

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) UpdateUser() {
	c.Obs.Logger.Info("incoming request to update user")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	updateUser := models.User{}
	err := c.Fiber.BodyParser(&updateUser)
	if err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := services.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.UpdateUserServices(updateUser)

	// Nonsense cache usage example
	c.Cache.SetJSON("latestupdateUser", updateUser, 0)
	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) DeleteUser() {
	c.Obs.Logger.Info("incoming request to delete user")
	response := models.Response{
		Code:    "01",
		Message: "Failed",
	}
	deleteUser := models.User{}
	err := c.Fiber.BodyParser(&deleteUser)
	if err != nil {
		c.Obs.Logger.Error(fmt.Sprintf("Failed to parsing body request: %s", err.Error()))
		c.Fiber.JSON(response)
		return
	}

	svc := services.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response = svc.DeleteUserServices(deleteUser)

	// Nonsense cache usage example
	c.Cache.SetJSON("latestdeleteUser", deleteUser, 0)

	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}

func (c *Initial) GetUser() {
	c.Obs.Logger.Info("incoming request to to list al; users")
	svc := services.Initial{
		Obs:   c.Obs,
		DB:    c.DB,
		Cache: c.Cache,
	}
	response := svc.GetUserServices()
	c.Fiber.JSON(response)
	c.Fiber.SendStatus(http.StatusOK)
}
