package middlewares

import (
	"fmt"
	"os"
	"toolkit/app/core/toolkit"

	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v2"
	"github.com/gofiber/fiber"
	_ "github.com/golang-migrate/migrate/v4/database/mysql"
)

var (
	adapter *gormadapter.Adapter
)

func init() {
	var err error
	toolkit.MustLoadEnvs("")
	adapter, err = gormadapter.NewAdapter("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/", os.Getenv("DATABASE_USER"), os.Getenv("DATABASE_PASSWORD"), os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_PORT")))
	if err != nil {
		panic(fmt.Sprintf("failed to initialize casbin adapter: %v", err))
	}
}

func CheckPermission(c *fiber.Ctx) {
	cas, err := casbin.NewEnforcer("rbac_model.conf", adapter)
	if err != nil {
		fmt.Println("Failed to create new enforce: ", err)
	}
	cas.LoadPolicy()
	ok, err := cas.Enforce(c.Cookies("user"), c.Path(), c.Method())
	if err != nil {
		fmt.Println("Error")
	}
	if !ok {
		c.JSON(map[string]string{"msg": "This user doesn't have permission"})
		return
	}
	cas.SavePolicy()
	c.Next()
}
