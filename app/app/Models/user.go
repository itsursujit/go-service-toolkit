package models

type Response struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data, omitempty"`
}

// CreateUser holds all basic user information.
type CreateUser struct {
	ID       uint64 `json:"id" gorm:"column:id"`
	Name     string `json:"name" validate:"required" gorm:"column:name"`
	Password string `json:"password" gorm:"column:password"`
	RoleID   uint64 `json:"roleId" gorm:"column:roleId"`
}

// User holds all basic user information.
type User struct {
	ID       uint64 `json:"id" gorm:"column:id"`
	Name     string `json:"name" validate:"required" gorm:"column:name"`
	Password string `json:"password" gorm:"column:password"`
}

type GetUserData struct {
	ID   uint64 `json:"id" gorm:"column:id"`
	Name string `json:"name" gorm:"column:name"`
	Role string `json:"role" gorm:"column:role"`
}

func (t *User) TableName() string {
	return "users"
}

type Account struct {
	ID   uint64 `json:"id" gorm:"column:id"`
	Name string `json:"name" gorm:"column:name"`
}

func (t *Account) TableName() string {
	return "accounts"
}

type UserRole struct {
	ID        uint64 `json:"id" gorm:"column:id"`
	UserID    uint64 `gorm:"column:user_id"`
	AccountID uint64 `gorm:"column:account_id"`
	RoleID    uint64 `gorm:"column:role_id"`
}

func (t *UserRole) TableName() string {
	return "user_roles"
}
