package models

type Role struct {
	ID          uint64       `json:"id" gorm:"column:id"`
	Name        string       `json:"name" gorm:"column:name"`
	Permissions []Permission `json:"permissions" gorm:"column:permissions"`
}

func (t *Role) TableName() string {
	return "roles"
}

// Policy ..
type Permission struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}
