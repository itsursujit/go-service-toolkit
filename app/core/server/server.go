package server

import (
	"context"
	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	"github.com/gofiber/helmet"
	"github.com/gofiber/template/handlebars"
	"github.com/pkg/errors"
	"os"
	"os/signal"
	"syscall"
	"time"
	"toolkit/app/core/observance"
)

const defaultTimeout = 30 * time.Second

func NewFiber(obs *observance.Obs, timeout ...string) (*fiber.App, error) {
	timeoutDuration := defaultTimeout
	if len(timeout) > 0 {
		parsedTimeout, err := time.ParseDuration(timeout[0])
		if err != nil {
			return nil, errors.Wrap(err, "timeout could not be parsed")
		}
		timeoutDuration = parsedTimeout
	}
	srv := fiber.New()
	srv.Settings.DisableStartupMessage = true
	// srv.Settings.Prefork = true
	srv.Settings.IdleTimeout = timeoutDuration
	srv.Settings.ReadTimeout = timeoutDuration
	srv.Settings.WriteTimeout = timeoutDuration
	srv.Settings.ServerHeader = "Verify-Rest"

	srv.Use(middleware.Recover())
	srv.Use(middleware.Compress(4))
	srv.Use(middleware.RequestID())
	srv.Use(helmet.New())

	srv.Static("/assets", "./assets", fiber.Static{
		Compress:  true,
		ByteRange: true,
	})
	srv.Settings.Views = handlebars.New("./resources/templates", ".html")
	// Set up graceful shutdown.
	connsClosed := make(chan struct{})
	sc := make(chan os.Signal)
	go func() {
		s := <-sc
		obs.Logger.WithField("signal", s).Warn("shutting down gracefully")

		_, cancel := context.WithTimeout(context.Background(), 9*time.Second)
		defer cancel()

		err := srv.Shutdown()
		if err != nil {
			obs.Logger.Error(err)
		}
		close(connsClosed)
	}()
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM)

	return srv, nil
}
