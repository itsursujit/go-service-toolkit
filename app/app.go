package app

import (
	"fmt"
	"os"
	"time"
	controllers "toolkit/app/app/Http/Controllers"
	"toolkit/app/core/cache"
	"toolkit/app/core/observance"
	"toolkit/app/core/toolkit"

	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

// User holds all basic user information.
type User struct {
	ID   uint64 `json:"id"`
	Name string `json:"name" validate:"required"`
	Age  uint64 `json:"age" validate:"gte=18"`
}

func Serve() {
	// Load environment variables.
	toolkit.MustLoadEnvs("")

	// Set up observance (logging).
	obsConfig := toolkit.ObsConfig{
		AppName:  os.Getenv("APP_NAME"),
		LogLevel: os.Getenv("LOG_LEVEL"),
		// SentryURL:            os.Getenv("SENTRY_URL"),
		Version: os.Getenv("APP_VERSION"),
		// MetricsURL:           os.Getenv("METRICS_URL"),
		MetricsFlushInterval: 1 * time.Second,
		LoggedHeaders: map[string]string{
			"FastBill-RequestId": "requestId",
		},
	}
	obs := toolkit.MustNewObs(obsConfig)
	defer obs.PanicRecover()

	// Set up DB connection and run migrations.
	dbConfig := toolkit.DBConfig{
		Dialect:  os.Getenv("DB_DIALECT"),
		Host:     os.Getenv("DATABASE_HOST"),
		Port:     os.Getenv("DATABASE_PORT"),
		User:     os.Getenv("DATABASE_USER"),
		Password: os.Getenv("DATABASE_PASSWORD"),
		Name:     os.Getenv("DATABASE_NAME"),
	}
	db := toolkit.MustSetupDB(dbConfig, obs.Logger)
	defer func() {
		if err := db.Close(); err != nil {
			obs.Logger.WithError(err).Error("failed to close DB connection")
		}
	}()

	toolkit.MustEnsureDBMigrations("migrations", dbConfig)

	// Set up REDIS newCache.
	newCache := toolkit.MustNewCache(os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT"), "testPrefix")
	defer func() {
		if err := newCache.Close(); err != nil {
			obs.Logger.WithError(err).Error("failed to close REDIS connection")
		}
	}()

	// Set up the server.
	addr := ":" + os.Getenv("PORT")
	startFiberRoutes(addr, obs, db, newCache)

}

func startFiberRoutes(addr string, obs *observance.Obs, db *gorm.DB, cache cache.Cache) {
	e, _ := toolkit.MustNewFiberServer(obs)
	controller := controllers.Initial{
		Obs:   obs,
		DB:    db,
		Cache: cache,
	}
	uiRedirect(e)
	// e.Use("/dashboard", middlewares.CheckPermission)
	// Set up a routes and handlers.
	// e.Use("/users", middlewares.CheckPermission)
	e.Post("/users", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.CreateUser()
	})
	e.Get("/users", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.GetUser()
	})
	e.Put("/users", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.UpdateUser()
	})
	e.Delete("/users", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.DeleteUser()
	})

	// e.Use("/roles", middlewares.CheckPermission)
	e.Post("/roles", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.CreateRole()
	})
	e.Get("/roles", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.GetRole()
	})
	e.Put("/roles", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.UpdateRole()
	})
	e.Delete("/roles", func(c *fiber.Ctx) {
		controller.Fiber = c
		controller.DeleteRole()
	})

	e.Get("/", func(c *fiber.Ctx) {
		c.Render("index", fiber.Map{})
	})
	fmt.Println("Listen address: ", addr)
	e.Listen(addr)
}

func uiRedirect(e *fiber.App) {
	e.Get("/dashboard/home", func(c *fiber.Ctx) {
		c.Redirect("/#/dashboard/home")
	})
}
