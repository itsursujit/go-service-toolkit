module toolkit

go 1.14

require (
	github.com/CloudyKit/jet v2.1.2+incompatible // indirect
	github.com/alicebob/miniredis/v2 v2.11.4
	github.com/casbin/casbin/v2 v2.7.1
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/certifi/gocertifi v0.0.0-20200211180108-c7c1fbc02894 // indirect
	github.com/evalphobia/logrus_sentry v0.8.2
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/gofiber/fiber v1.11.1
	github.com/gofiber/helmet v0.1.0
	github.com/gofiber/template v1.5.2
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/jinzhu/gorm v1.9.13
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.10.9 // indirect
	github.com/labstack/gommon v0.3.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.7.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
	google.golang.org/protobuf v1.24.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
